/**
 * Proxy class that allows an EventSource to be restarted without losing the set event listeners
 *
 * @implements EventSource
 */
export default class RestartableEventSource {

  CLOSED = EventSource.CLOSED;
  CONNECTING = EventSource.CONNECTING;
  OPEN = EventSource.OPEN;

  get onmessage() {
    return this._eventSource.onmessage;
  }

  set onmessage(f) {
    this._eventSource.onmessage = this._onmessage = f;
  }

  get onopen() {
    return this._eventSource.onopen;
  }

  set onopen(f) {
    this._eventSource.onopen = this._onopen = f;
  }

  get readyState() {
    return this._eventSource.readyState;
  }

  get url() {
    return this._eventSource.url;
  }

  get withCredentials() {
    return this._eventSource.withCredentials;
  }

  /**
   * @param {string} url
   * @param {EventSourceInit} [configuration]
   */
  constructor(url, configuration) {
    this._listeners = new Map();
    this._onerror = null;
    this._onmessage = null;
    this._onopen = null;
    this._arguments = { url, configuration };

    this.restart();
  }

  close() {
    this._eventSource.close();
  }

  restart() {
    if (typeof this._eventSource !== 'undefined') {
      this._eventSource.close();
    }

    this._eventSource = new EventSource(this._arguments.url, this._arguments.configuration);
    this._eventSource.onerror = this._onerror;
    this._eventSource.onmessage = this._onmessage;
    this._eventSource.onopen = this._onopen;

    this._listeners.forEach((listeners, type) => {
      listeners.forEach(listener => {
        this._eventSource.addEventListener(type, listener);
      });
    });
  }

  addEventListener(type, listener) {
    this._eventSource.addEventListener(type, listener);

    if (!this._listeners.has(type)) {
      this._listeners.set(type, []);
    }

    this._listeners.get(type).push(listener);
  }

  removeEventListener(type, listener) {
    this._eventSource.removeEventListener(type, listener);

    if (!this._listeners.has(type)) {
      return;
    }

    const stack = this._listeners.get(type);
    for (let i = stack.length - 1; i <= 0; i--) {
      if (stack[i] === listener) {
        stack.splice(i, 1);
      }
    }
  }
}
