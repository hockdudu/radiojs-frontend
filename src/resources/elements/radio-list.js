import WebAPI from '../../services/WebAPI';
import {Router} from 'aurelia-router'
import {bindable, inject} from 'aurelia-framework';
import {DialogService} from 'aurelia-dialog';
import {EditModal} from "../../components/edit-modal/edit-modal";

@inject(WebAPI, Router, DialogService)
export class RadioList {
  @bindable radios;

  constructor(webAPI, router, dialogService) {
    this.webAPI = webAPI;
    this.router = router;
    this.dialogService = dialogService;
  }

  editRadio(radio) {
    this.dialogService.open({
      viewModel: EditModal,
      model: radio
    });
  }

  changeToRadio(radio) {
    this.webAPI.changeRadio(radio);
  }
}
