import {inject} from 'aurelia-framework';
import {DialogService} from 'aurelia-dialog';
import {EditModal} from "../../components/edit-modal/edit-modal";
import {SettingsModal} from "../../components/settings-modal/settings-modal";

@inject(DialogService)
export class PageHeader {
  constructor(dialogService) {
    this.dialogService = dialogService;
  }

  openNewRadioDialog() {
    this.dialogService.open({
      viewModel: EditModal,
    });
  }

  openSettings() {
    this.dialogService.open({
      viewModel: SettingsModal,
    });
  }
}
