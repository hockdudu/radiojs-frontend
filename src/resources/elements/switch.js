import {bindable} from "aurelia-templating";

export class Switch {
  @bindable
  enabled;
}
