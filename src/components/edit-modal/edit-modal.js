import {inject, computedFrom} from 'aurelia-framework';
import {DialogController} from 'aurelia-dialog';
import Tagify from "@yaireo/tagify";
import WebAPI from "../../services/WebAPI";

@inject(DialogController, WebAPI)
export class EditModal {
  @computedFrom('radio')
  get isNew() {
    return typeof this.radio.id === 'undefined';
  }

  radio;

  constructor(controller, webAPI) {
    this.controller = controller;
    this.webAPI = webAPI;
  }

  activate(radio) {
    if (typeof radio !== 'undefined' && radio !== null) {
      this.radio = radio;
    } else {
      this.radio = {
        name: '',
        url: '',
        tags: [],
      };
    }
  }

  attached() {
    this.tagify = new Tagify(this.tagsSelect);
    this.initializeChoices();
  }

  initializeChoices() {
    this.webAPI.getRadioList().then(radioList => {
      const tags = EditModal.getTagsFromRadios(radioList);
      this.tagify.settings.whitelist.length = 0;
      this.tagify.settings.whitelist.push(...tags);
    });

    this.tagify.addTags(this.radio.tags)
  }

  async submit(e) {
    this.injectTagsToRadio();

    if (this.isNew) {
      await this.webAPI.newRadio(this.radio);
    } else {
      await this.webAPI.modifyRadio(this.radio);
    }

    this.controller.ok();
  }

  async deleteRadio() {
    await this.webAPI.deleteRadio(this.radio);
    this.controller.ok();
  }

  injectTagsToRadio() {
    const tags = this.tagify.value;
    this.radio.tags = tags.map(tag => tag.value);
  }

  static getTagsFromRadios(radios) {
    const tags = radios.map(radio => radio.tags).reduce((array, tags) => array.concat(tags));
    return tags.filter((tag, index, array) => array.indexOf(tag) === index);
  }
}
