import {inject} from 'aurelia-framework';
import {DialogController} from 'aurelia-dialog';
import NotificationService from "../../services/NotificationService";

@inject(DialogController, NotificationService)
export class SettingsModal {

  /**
   *
   * @param {DialogController} dialogController
   * @param {NotificationService} notificationService
   */
  constructor(dialogController, notificationService) {
    this.controller = dialogController;
    this.notificationService = notificationService;
  }

  toggleNotifications() {
    this.notificationService.toggleNotifications();
  }
}
