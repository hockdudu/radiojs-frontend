// regenerator-runtime is to support async/await syntax in ESNext.
// If you don't use async/await, you can remove regenerator-runtime.
import 'regenerator-runtime/runtime';
import * as environment from '../config/environment.json';
import {PLATFORM} from 'aurelia-pal';

export function configure(aurelia) {
  aurelia.use
    .standardConfiguration()
    .feature(PLATFORM.moduleName('resources/index'));

  aurelia.use.developmentLogging(environment.debug ? 'debug' : 'warn');

  if (environment.testing) {
    aurelia.use.plugin(PLATFORM.moduleName('aurelia-testing'));
  }

  aurelia.use.plugin(PLATFORM.moduleName('aurelia-dialog'), config => {
    config.useRenderer('ux');
    config.useCSS('');
    config.useResource('attach-focus');
    // config.settings.lock = false;
    config.settings.overlayDismiss = true;
    config.settings.keyboard = ['Escape'];
  });

  aurelia.start().then(() => aurelia.setRoot(PLATFORM.moduleName('app')));
}
